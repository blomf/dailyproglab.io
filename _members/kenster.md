---
layout: member
title: kenster
handle: kenster
nationality: Czech-American 🇨🇿/🇺🇸
specializations: Nim, TypeScript & JavaScript, C++, Python, Common Lisp 👽
---

Full stack software engineer and entrepeneur that's well-qualified for meme-dev; however, he is mainly passionate about operating systems, software design, and software architecture.  
  
He occasionally does programming livestreams.  
  
[Youtube channel](https://www.youtube.com/user/KingHerring)
[Twitch channel](https://www.twitch.tv/kingherring)
[Github](https://github.com/kennymalac/)
