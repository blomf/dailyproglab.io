---
layout: member
title: d3d1rty
handle: d3d1rty
nationality: American 🇺🇸
specializations: Ruby, Javascript, SCSS
---

RV Davis (d3d1rty) is an entrepreneurial-minded Software Engineer skilled in building web apps & APIs, scripts, bots, and more. He has previously served in the United States Army Infantry for 5 years, deploying twice to Afghanistan in support of Operation Enduring Freedom (OEF X-XI and OEF XIII).

Email: dick AT rvdavis DOT me

[Public Key](https://pgp.mit.edu/pks/lookup?op=get&search=0x0997BADFB2E6E15E) : A7F7 F47B C0DD 7C37 C943 AD99 0997 BADF B2E6 E15E

[Blog](https://rvdavis.me)
|
[GitLab](https://gitlab.com/d3d1rty)
|
[GitHub](https://github.com/d3d1rty)
|
[LinkedIn](https://www.linkedin.com/in/rvdavis/)
