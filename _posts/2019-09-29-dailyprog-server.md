---
layout: post
title:  "Server EOL"
date:   2019-09-29 18:00:00 -0500
categories: announcements
---

The dailyprog server is shutting down tomorrow. I will point the DNS for dailyprog.org to this site after it goes down; I haven't done so yet so that everyone has a chance to get what they need off the server. If you didn't get a chance to back up your data, get with ryukoposting as he may have a copy of what you are looking for.

In other news, we have made steady progress on the new version of the dailyprog site (this blog). We have been striving to remain close to the aesthetic of the original site, yet open to making some improvements. The most recent addition is an optimized pantheon header for our patron saints of programming. The next major thing that we'd like to implement is a "Projects" section to showcase our members' work.

If you have any recommendations for changes or features to enhance the new dailyprog site, feel free to send me an email, drop by our IRC channel, or open a merge request.

[d3d1rty](/members/d3d1rty)
