---
layout: default
name: Brendan Eich
image-url: "/assets/eich.png"
wiki-url: https://en.wikipedia.org/wiki/Brendan_Eich
blurb: is an American technologist and creator of the JavaScript programming language.
---
