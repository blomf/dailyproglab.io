---
layout: default
title: Shrine
permalink: /shrine/
---

{% for saint in site.saints %}
  ![]({{saint.image-url}}){:class="Saint__img"}

  [{{ saint.name}}]({{ saint.wiki-url }}) - {{ saint.blurb }}
{% endfor %}
