---
layout: member
title: anon1
handle: anon1
nationality: German 🇩🇪
specializations: C/C++, Python, Arduino, VHDL
---

anon1 is currently doing his masters Computer Enginnering and usually spends
his free time building robots or some other hardware things.

Current projects: 

- A 3D printed quadruped robot in C++ using a raspberry
- A CNC machine to improve the manufacturing of homemade PCBs.

<video width="640" controls>
  <source src="https://i.imgur.com/BwR9GiZ.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
