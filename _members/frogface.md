---
layout: member
title: frogface
handle: frogface
nationality: Bulgarian 🇧🇬
specializations: Bash, Python and C
---

- Ham radio aficionada 
- wireless and wired network specialist
- sysadmin
- infosec
- cats 🐾