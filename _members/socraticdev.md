---
layout: member
title: socraticdev
handle: socraticdev
nationality: Canadian 🇨🇦
specializations: OOP, TDD, C#, SQL, JavaScript, Python, Hadoop ecosystem
---

Learning new stuff such as big data stuff, computer science stuff, 
JavaScript stuff and python stuff.

Working on various systems and technologies  

- ASP MVC web apps;
- restful web services;
- databases : querying, scripting and stored procedures;
- batch jobs.

homepage : [https://en.maximebonin.dev](https://en.maximebonin.dev)

blog : [https://en.socratic.dev](https://en.socratic.dev)
