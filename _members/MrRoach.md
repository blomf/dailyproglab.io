---
layout: member
title: Mr Roach
handle: MrRoach
nationality: American 🇺🇸
specializations: Go, Javascript, SQL, C/C++
---

# About Mr Roach
Mr Roach has been programming mostly as a hobby since around 2009, starting out by writing JavaScript plugins for [ScriptBot](https://github.com/onekopaka/ScriptBot), a mostly dormant IRC bot written in JavaScript and using [PircBot](https://github.com/davidlazar/PircBot) for the backend IRC stuff.

Since then, he's been writing JavaScript games and projects for the [(mini)Sphere](http://www.spheredev.org/) game engine, working on an imageboard called [gochan](http://gochan.org/) written in Go, a cross-platform IDE for miniSphere, and various other small projects.

[GitHub](https://github.com/eggbertx)
|
[GitHub projects of note](https://eggbertx.github.io/)