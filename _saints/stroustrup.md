---
layout: default
name: Bjarne Stroustrup
image-url: "/assets/stroustrup.png"
wiki-url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup
blurb: Bjarne Stroustrup is a Danish computer scientist, most notable for the creation and development of the C++ programming language.
---
