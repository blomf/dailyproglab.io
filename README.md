# dailyprog
This project is the website for the #dailyprog IRC channel on Rizon.

## Getting Started
Ensure that you have a [working Ruby environment](https://jekyllrb.com/docs/installation/) on your local machine.

Install the Jekyll ruby gem.

```
gem install jekyll bundler
```

Clone the repo.

```
git clone git@gitlab.com:dailyprog/dailyprog.gitlab.io.git
cd dailyprog.gitlab.io
```

Start the Jekyll server.

```
bundle exec jekyll serve
```

## Add a Member Page
Create a markdown document in the `_members/` directory following this format:

```
---
layout: member
title: [YOUR IRC NICK]
handle: [YOUR IRC NICK]
nationality: [NATIONALITY FLAG (ex. USA 🇺🇸)]
specializations: [LIST YOUR SPECIALIZATIONS]
---

[YOUR MARKDOWN-FORMATTED CONTENT HERE]
```

The only required variables in the front matter are title and handle. The content that you place following the front-matter should be in markdown format, though you can also use inline HTML as well. See other member pages for an idea of what you can do.

## Contributing
All dailyprog members should have full owner access for the repo. Anyone not in the dailyprog GitLab group should either request access from a member or simply fork the repo and submit a merge request.

Some guidelines for contributions:
* Always ask for a code review. Give some time for someone to get to it. The exception to this is with copy updates such as new posts or member pages (though it still helps to have someone read over it!)
* Ask someone to pair with you. Pairing is great for new and experienced programmers alike.
* Be open to criticism; be fair when giving it.
* Keep your merge requests constrained to the primary scope for the change you are trying to make. Keep the changes as small and atomic as possible.
* Use the Kanban board!
* Use [BEM syntax](http://getbem.com/introduction/) for CSS class names (ex. `Block__element--modifier`).
* Favor readability over conciseness, but strive for both.
