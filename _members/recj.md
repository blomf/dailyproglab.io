---
layout: member
title: recj
handle: recj
nationality: American 
specializations: C, Haskell, Python, Scheme
---


[Public Key](#) : TBD

[Blog](https://rvdavis.me)
|
[GitLab](https://gitlab.com/recj)
|
[GitHub](https://github.com/rodjoseph)
