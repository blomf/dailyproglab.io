---
layout: post
title:  "The New Dailyprog"
date:   2019-09-12 18:00:00 -0500
categories: announcements
---

Dailyprog is in a state of transition. Many of the original members of dailyprog have moved on, including some of the founding members. Though we will sorely miss him, the loss of one of our founding members has resulted in the impending expiration of our servers at the end of the month.

The current state of affairs is unsustainable, so we are moving in a slightly different direction.

Instead of self-hosting our website on a dedicated server, we will be hosting our site in a [GitLab repository](https://gitlab.com/dailyprog/dailyprog.gitlab.io/) published through the GitLab Pages service. The site will be generated using Jekyll, and all members are free to contribute as they like.

This approach has some benefits.

First, we will achieve separation between our public-facing website and the sandbox/hosted services. This has been a critical vulnerability in the past given how freely we have handed out `sudo` privileges to members and even visitors.

Second, we will make it easier to publish and track changes to the site. Any member of our GitLab group will be able to submit merge requests to make changes to the site (including their personal member page). This is a significant improvement to the somewhat convoluted and insecure way that the site was maintained previously.

This takes care of the web site, but what will we do about the sandbox/hosted services? Where will the bots live?

While we don't have a definitive answer yet, we do have a general idea of what we'd like to do: Hackerchan.

Hackerchan is a group project that has been on the proverbial backburner for the past year. When completed, it will provide an IRC server, imageboard, and sandbox for developers to collaborate with each other. Dailyprog is a community, and Hackerchan is its future home.

Dailyprog will continue to exist as an IRC-based community, but we will transition from Rizon to Hackerchan once the project has been completed. In the meantime, our members can make full use of the Hackerchan sandbox and are highly encouraged to assist in the development of the project.

The Hackerchan server will be up through the end of October at its current location. We don't know if we will continue to host it where it currently exists, or if we'll migrate it to a different provider, but we will make an announcement when the time comes.

If you have any questions or concerns, drop by #dailyprog on Rizon and let's talk.

[d3d1rty](/members/d3d1rty)
