---
layout: default
name: Paul Graham
image-url: "/assets/pg.png"
wiki-url: https://en.wikipedia.org/wiki/Paul_Graham_(programmer)
blurb: Paul Graham is an English-born computer scientist, entrepreneur, venture capitalist, author, and essayist.
---
