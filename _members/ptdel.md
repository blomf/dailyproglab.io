---
layout: member
title: ptdel
handle: ptdel
nationality: American 🇺🇸
specializations: Nim, Haskell, F#, C#, Python, Azure, AWS, CICD
---

A developer that enjoys developing tools that help developers develop.  I don't know anything about Excel.  Concerned with reliability, maintainability, portability, readability, and so many other things ending in -ability.  Good at whatever it is people are paying good money for.

- Github: [@ptdel](https://github.com/ptdel)