---
layout: member
title: bard
handle: bard
nationality: American 🇺🇸
specializations: Python, Scheme
---

Free software advocate. He believes many things need to be torn down and rebuilt.

[Fediverse](https://weeaboo.space/users/brad)
